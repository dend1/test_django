from django import forms
from django.forms import TextInput
from .models import Posts


class PostsForm(forms.ModelForm):

    class Meta:
        model = Posts
        fields = ('title', 'text')
        widgets = {
            'title': TextInput(attrs={'class': 'input'}),
            'text': TextInput(attrs={'class': 'input'}),
        }
