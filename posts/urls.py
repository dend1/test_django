from django.urls import path
from .views import *

urlpatterns = [
    path('', MainPage.as_view(), name='mainPage'),
    path('<int:pk>', FullPost.as_view(), name='fullPost'),
    path('<int:pk>/edit', EditPost.as_view(), name='editPost'),
]
