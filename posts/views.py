from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from .models import Posts
from .forms import PostsForm


class MainPage(View):

    @staticmethod
    def get(request):
        return render(request, 'posts/posts.html', {
            'posts': Posts.objects.all(),
        })


class FullPost(View):

    def dispatch(self, request, *args, **kwargs):
        if self.request.POST.get('_method', '').lower() == 'delete':
            return self.delete(request, kwargs['pk'])
        return super(FullPost, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get(request, pk=None):
        return render(request, 'posts/full_post.html', {
            'post': Posts.objects.get(id=pk)
        })

    @staticmethod
    def delete(request, pk=None):
        Posts.objects.get(id=pk).delete()
        return redirect('mainPage')


class EditPost(View):

    def dispatch(self, request, *args, **kwargs):
        if self.request.POST.get('_method', '').lower() == 'delete':
            return self.delete(request, kwargs['pk'])
        return super(EditPost, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get(request, pk=None):
        return render(request, 'posts/edit.html', {
            'post': Posts.objects.get(id=pk),
            'form': PostsForm()
        })

    def post(self, request, pk=None):
        form = PostsForm(request.POST)
        if form.is_valid():
            post = get_object_or_404(Posts, pk=pk)
            post.text = form.cleaned_data['text']
            post.title = form.cleaned_data['title']
            post.save()
            return redirect('fullPost', pk=pk)
        else:
            return self.get(request, pk)

    @staticmethod
    def delete(request, pk=None):
        Posts.objects.get(id=pk).delete()
        return redirect('mainPage')
