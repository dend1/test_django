from django.db import models


class Tags(models.Model):
    class Meta:
        db_table = 'tags'
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
        ordering = ('id',)

    id = models.AutoField(primary_key=True)
    name = models.TextField(max_length=10)

    def __str__(self):
        return f'{self.name}'


class Posts(models.Model):
    class Meta:
        db_table = 'posts'
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = ('id', )

    id = models.AutoField(primary_key=True)
    title = models.TextField(max_length=30)
    text = models.TextField(max_length=100)
    date = models.TextField()
    tag = models.ManyToManyField(Tags)

    def __str__(self):
        return f'{self.title}'
